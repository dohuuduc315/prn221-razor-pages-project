﻿using BusinessObjects.Entities;

namespace Repositories
{
    public interface IMovieRepository : IGenericRepository<Movie>
    {
    }
}
