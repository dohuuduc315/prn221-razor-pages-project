﻿using BusinessObjects.Entities;

namespace Repositories
{
    public interface IAccountRepository : IGenericRepository<Account>
    {
        public Task<Account> LoginAsync(string username, string password);
    }
}
