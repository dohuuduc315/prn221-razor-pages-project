﻿using BusinessObjects;
using BusinessObjects.Entities;
using Microsoft.EntityFrameworkCore;

namespace Repositories
{
    public class AccountRepository : GenericRepository<Account>, IAccountRepository
    {
        public AccountRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<Account> LoginAsync(string username, string password)
        {
            return await _context.Accounts.FirstOrDefaultAsync(x => x.Username.Equals(username) && x.Password.Equals(password));
        }
    }
}
