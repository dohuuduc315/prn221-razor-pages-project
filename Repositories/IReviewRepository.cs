﻿using BusinessObjects.Entities;

namespace Repositories
{
    public interface IReviewRepository : IGenericRepository<Review>
    {
    }
}
