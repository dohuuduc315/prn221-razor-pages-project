﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BusinessObjects.Entities;
using BusinessObjects;
using Repositories.Commons;

namespace MovieWeb.Pages.Reviews
{
    public class IndexModel : PageModel
    {
        private readonly AppDbContext _context;
        private readonly IConfiguration _configuration;

        public IndexModel(IConfiguration configuration, AppDbContext context)
        {
            _context = context;
            _configuration = configuration;
        }

        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }

        public PaginatedList<Review> Review { get;set; } = default!;

        public async Task OnGetAsync(string sortOrder, string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString is not null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            CurrentFilter = searchString;

            IQueryable<Review> reviewIQ = from r in _context.Reviews select r;

            if (!String.IsNullOrEmpty(searchString))
            {
                reviewIQ = reviewIQ.Where(x => x.Title.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    reviewIQ = reviewIQ.OrderByDescending(x => x.Title);
                    break;
                default:
                    reviewIQ = reviewIQ.OrderBy(x => x.Title);
                    break;
            }

            var pageSize = _configuration.GetValue("PageSize", 4);
            Review = await PaginatedList<Review>.CreateAsync(reviewIQ.AsNoTracking(), pageIndex ?? 1, pageSize);
        }
    }
}
