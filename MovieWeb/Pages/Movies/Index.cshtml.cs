﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using BusinessObjects.Entities;
using Repositories.Commons;
using BusinessObjects;
using Microsoft.EntityFrameworkCore;

namespace MovieWeb.Pages.Movies
{
    public class IndexModel : PageModel
    {
        private readonly AppDbContext _context;
        private readonly IConfiguration _configuration;

        public IndexModel(IConfiguration configuration, AppDbContext context)
        {
            _context = context;
            _configuration = configuration;
        }

        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }

        public PaginatedList<Movie> Movie { get;set; } = default!;

        public async Task OnGetAsync(string sortOrder, string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString is not null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            CurrentFilter = searchString;

            IQueryable<Movie> movieIQ = from m in _context.Movies select m;

            if (!String.IsNullOrEmpty(searchString))
            {
                movieIQ = movieIQ.Where(x => x.Name.Contains(searchString));
            }
            
            switch (sortOrder)
            {
                case "name_desc":
                    movieIQ = movieIQ.OrderByDescending(x => x.Name);
                    break;
                default:
                    movieIQ = movieIQ.OrderBy(x => x.Name);
                    break;
            }

            var pageSize = _configuration.GetValue("PageSize", 4);
            Movie = await PaginatedList<Movie>.CreateAsync(movieIQ.AsNoTracking(), pageIndex ?? 1, pageSize);
        }
    }
}
