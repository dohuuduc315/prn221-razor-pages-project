﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using BusinessObjects.Entities;
using BusinessObjects;
using Repositories.Commons;

namespace MovieWeb.Pages.Accounts
{
    public class IndexModel : PageModel
    {
        private readonly AppDbContext _context;
        private readonly IConfiguration _configuration;

        public IndexModel(IConfiguration configuration, AppDbContext context)
        {
            _context = context;
            _configuration = configuration;
        }

        public string NameSort { get; set; }
        public string DateSort { get; set; }
        public string CurrentFilter { get; set; }
        public string CurrentSort { get; set; }

        public PaginatedList<Account> Account { get;set; } = default!;

        public async Task OnGetAsync(string sortOrder, string currentFilter, string searchString, int? pageIndex)
        {
            CurrentSort = sortOrder;
            NameSort = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString is not null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            CurrentFilter = searchString;

            IQueryable<Account> accountIQ = from a in _context.Accounts select a;

            if (!String.IsNullOrEmpty(searchString))
            {
                accountIQ = accountIQ.Where(x => x.Username.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc":
                    accountIQ = accountIQ.OrderByDescending(x => x.Username);
                    break;
                default:
                    accountIQ = accountIQ.OrderBy(x => x.Username);
                    break;
            }

            var pageSize = _configuration.GetValue("PageSize", 4);
            Account = await PaginatedList<Account>.CreateAsync(accountIQ.AsNoTracking(), pageIndex ?? 1, pageSize);
        }
    }
}
