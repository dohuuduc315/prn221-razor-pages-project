using BusinessObjects.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Repositories;

namespace MovieWeb.Pages
{
    public class LoginModel : PageModel
    {
        private readonly IAccountRepository _accountRepository;

        public LoginModel(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }
        public void OnGet()
        {
        }

        [BindProperty]
        public Account Account { get; set; }
        public string msg { get; set; }

        public async Task<IActionResult> OnPost()
        {
            var login = await _accountRepository.LoginAsync(Account.Username, Account.Password);

            if (login is null)
            {
                msg = "Invalid email or password";
                return Page();
            }

            return RedirectToPage("./Movies/Index");
        }
    }
}
