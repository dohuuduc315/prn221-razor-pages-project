﻿namespace BusinessObjects.Entities
{
    public class Movie : BaseEntity
    {
        public Movie()
        {
            Reviews = new HashSet<Review>();
        }

        public string Name { get; set; }
        public string Director { get; set; }
        public double Rating { get; set; }
        public string? Description { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
    }
}
