﻿namespace BusinessObjects.Entities
{
    public class Review : BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public double Rating { get; set; }
        public virtual Movie? Movie { get; set; }
        public int MovieId { get; set; }
    }
}
